<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Facebook Poster</title>
        <base href='<?= URL::to('/puplic/') ?>' />
        <script>var base = '<?= URL::to('/puplic/') ?>';</script>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >

        <link rel="icon" href="favicon.ico" type="image/x-icon" >
        <!-- ./meta section -->

        <!-- css styles -->
        <link rel="stylesheet" type="text/css" href="./assets/backend/css/default-blue-white.css" id="dev-css">
        <!-- ./css styles -->                              

        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="./assets/backend/css/dev-other/dev-ie-fix.css">
        <![endif]-->

        <!-- javascripts -->
        <script src="./assets/backend/js/jquery.js"></script>
        <script type="text/javascript" src="./assets/backend/js/plugins/modernizr/modernizr.js"></script>
        <!-- ./javascripts -->

        <style>
            .dev-page{visibility: hidden;}            
        </style>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->       

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <div class="dev-page-header">

                <div class="dph-logo">
                    <a href="./dashboard" style="height: auto;zoom: .2;background-image:none;"></a>
                    <a class="dev-page-sidebar-collapse">
                        <div class="dev-page-sidebar-collapse-icon">
                            <span class="line-one"></span>
                            <span class="line-two"></span>
                            <span class="line-three"></span>
                        </div>
                    </a>
                </div>

                <ul class="dph-buttons pull-right" style="">                    
                    <li class="dph-button-stuck" style="display: none">
                        <a href="#" class="dev-page-search-toggle">
                            <div class="dev-page-search-toggle-icon">
                                <span class="circle"></span>
                                <span class="line"></span>
                            </div>
                        </a>
                    </li>                    
                    <li class="dph-button-stuck" >
                        <a href="#" class="sayed dev-page-rightbar-toggle">
                            <div class="dev-page-rightbar-toggle-icon">
                                <span class="notify-message" style="display: none"></span>
                                <span class="line-one"></span>
                                <span class="line-two"></span>
                            </div>
                        </a>
                    </li>
                </ul>                                                

            </div>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <div class="dev-page-sidebar">                    

                    <div class="profile profile-transparent">
                        <div class="profile-image">
                            <img src="./assets/backend/img/avatar.png">
                            <div class="profile-badges">
                            </div>
                            <div class="profile-status online"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Admin</h4>
                        </div>                        
                    </div>

                    <ul class="dev-page-navigation">
                        <li class="has-child">
                            <a href="#"><i class="fa fa-facebook-square"></i> <span>Facebook Bublisher</span></a>
                            <ul style="max-height: 0px;">
                                <li>
                                    <a href="./dashboard/facebook-publisher/login"><i class="fa fa-sign-in"></i> <span>login</span></a>
                                </li>
                                <li>
                                    <a href="./dashboard/facebook-publisher/pages"><i class="fa fa-paperclip"></i> <span>Pages</span></a>
                                </li>
                                <li>
                                    <a href="./dashboard/facebook-publisher/poster"><i class="fa fa-pencil-square"></i> <span>Poster</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">
                        <div class="wrapper">
                            @yield('content')
                        </div>
                    </div>
                    <!-- ./page content container -->

                </div>
                <!-- ./page content -->                                               
            </div>  
            <!-- ./page container -->

            <!-- right bar -->
            <div class="dev-page-rightbar">
                <div class="rightbar-chat">

                    <div class="rightbar-chat-frame-contacts scroll" style="height: 100%;">
                        <div class="rightbar-title">
                            <h3>Messages</h3>
                            <a href="#" class="btn btn-default btn-rounded rightbar-close pull-right"><span class="fa fa-times"></span></a>
                        </div>
                        <ul class="contacts">

                        </ul>
                        <div class="search-tankers">
                            <div class="form-group">
                                <div class="input-group" style="width:100%">
                                    <span class="search-tanker-icon"><i class="fa fa-search"></i></span>
                                    <input type="text" class="form-control" id="tanker_search" placeholder="search for tanker">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rightbar-chat-frame-chat mychat">
                        <div class="user">
                            <div class="user-panel">
                                <a href="#" class="btn btn-default btn-rounded rightbar-chat-close"><span class="fa fa-angle-left"></span></a>
                                <!--<a href="#" class="btn btn-default btn-rounded pull-right"><span class="fa fa-user"></span></a>-->
                            </div>
                            <div class="user-info">
                                <div class="user-info-image status online">
                                    <img src="" id="chat_user_image">
                                </div>
                                <h5 id="chat_user_name"></h5>
                            </div>
                        </div>
                        <div class="chat-wrapper scroll">
                            <ul class="chat" id="rightbar_chat">

                            </ul>
                        </div>

                        <form class="form" action="javascript:void(0)" method="post" id="rightbar_chat_form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="hidden" name="tanker_id" id='chat_tanker_id' />
                                    <input type="text" class="form-control" name="message">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- ./right bar -->

            <!-- page footer -->    
            <div class="dev-page-footer dev-page-footer-fixed"> <!-- dev-page-footer-closed dev-page-footer-fixed -->
                <ul class="dev-page-footer-controls">
                    <li><a href="./backend/logout" class="tip" title="Logoff"><i class="fa fa-power-off"></i></a></li>

                    <li class="pull-right">
                        <a class="dev-page-sidebar-minimize tip" title="Toggle navigation"><i class="fa fa-outdent"></i></a>
                    </li>
                </ul>

                <!-- page footer buttons -->
                <ul class="dev-page-footer-buttons">                    
                    <!--<li style="display: none;"><a href="#footer_content_1" class="dev-page-footer-container-open"><i class="fa fa-database"></i></a></li>-->                    
                    <!--<li><a href="#footer_content_2" class="dev-page-footer-container-open"><i class="fa fa-bar-chart"></i></a></li>-->
                    <!--<li style="display: none;"><a href="#footer_content_3" class="dev-page-footer-container-open"><i class="fa fa-server"></i></a></li>-->
                </ul>
                <!-- ./page footer buttons -->
                <!-- page footer container -->

                <!-- ./page footer container -->
				<a href="" class="notify">
					<!--<span class="notify-close" onclick="$(this).parent().fadeOut();">x</span>-->
					<div class="notify-content">
						bla la bla
					</div>
				</a>
				<style>
					.notify {
						display:none;
						position: fixed;
						bottom: 50px;
						right: 20px;
						z-index: 111111;
						border: 1px solid #2d3b50;
						background: #2d3b50;
						color:#f0f4f6;
						border-radius: 5px;
						font-weight: bold;
						font-size: 1em;
					}
					.notify:hover{
						text-decoration: none;
						font-size: 1.2em;
						color:#f0f4f6;
					}
					/*					.notify-close {
											position: absolute;
											top: 5px;
											right: 5px;
											text-align: center;
											width: 15px;
											height: 15px;
											padding: 2px;
											z-index: 111;
											color: #FF0000;
											cursor: pointer;
											line-height: 15px; 
										}*/
					.notify-content {
						height: auto;
						min-width: 300px;
						padding: 15px;
					}
				</style>
                <ul class="dev-page-footer-controls dev-page-footer-controls-auto pull-right">
                    <li><a class="dev-page-footer-fix tip" title="Fixed footer"><i class="fa fa-thumb-tack"></i></a></li>
                    <li><a class="dev-page-footer-collapse dev-page-footer-control-stuck"><i class="fa fa-dot-circle-o"></i></a></li>
                </ul>
            </div>
            <!-- ./page footer -->

            <!-- page search -->
            <div class="dev-search">
                <div class="dev-search-container">
                    <div class="dev-search-form">
						<form action="index.html" method="post">
							<div class="dev-search-field">
								<input type="text" placeholder="Search..." value="Nature">
							</div>                        
						</form>
                    </div>
                    <div class="dev-search-results"></div>
                </div>
            </div>
            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->

        <!-- javascript -->
        <script type="text/javascript">

			$(document).on('change', 'select', function(e) {
				if ($(this).find('option:selected').val() != "") {
					var selectname = $(this).attr('name').replace('[', '\\[').replace(']', '\\]');
					$('#' + selectname + '-error').hide();
				}
			});
        </script>
        <script type="text/javascript" src="./assets/backend/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="./assets/backend/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>        
        <script type="text/javascript" src="./assets/backend/js/plugins/moment/moment.js"></script>
        <script type="text/javascript" src="./assets/backend/js/plugins/knob/jquery.knob.min.js"></script>
        <script type="text/javascript" src="./assets/backend/js/plugins/sparkline/jquery.sparkline.min.js"></script>
        
        <!--sweet alert-->
        <script src="./libraries/sweet-alert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./libraries/sweet-alert/dist/sweetalert.css">
        
        @yield('js')

        <script type="text/javascript" src="./assets/backend/js/dev-loaders.js"></script>
        <!--<script type="text/javascript" src="./assets/backend/js/dev-settings.js"></script>-->
        <script type="text/javascript" src="./assets/backend/js/dev-layout-default.js"></script>
        <script type="text/javascript" src="./assets/backend/js/dev-app.js"></script>
        <script type="text/javascript" src="./assets/backend/js/demo.js"></script>
        <!--<script type="text/javascript" src="./assets/backend/js/demo-dashboard.js"></script>-->
        <!--<script type="text/javascript" src="./assets/backend/js/demo-charts.js"></script>-->

        <!-- ./javascript -->






    </body>
</html>






