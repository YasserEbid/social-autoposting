﻿@extends('backend.layout')
@section('content')

<div class="page-title">
    <h1>New {{$module_name}}</h1>

    <ul class="breadcrumb">
        <li><a href="./dashboard">Dashboard</a></li>
        <li><a href="./dashboard/{{$module_url}}/pages">{{$module_name}}/pages</a></li>
        <li>Add New</li>
    </ul>
</div>

<div class="wrapper wrapper-white">

    <div class="row">
        @if(Session::has('errors'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error !</strong> 
                    <br/>
                    {!!Session::get('errors')!!}
            </div>
        @endif 
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Success !</strong> 
                    <br/>
                    {{Session::get('success')}}
            </div>
        @endif
        
        
        <form method="post" id="validate" role="form" action="">
            <div class="col-md-12 col-sm-12">
                <h3>{{$row->name}}</h3>
            </div>
                
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label>time :</label>
                    <input type="number" name="time" value="{{$row->time}}" class="form-control" placeholder="15.30.45.60......" />
                </div>
            </div>
            
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label>is active :</label>
                    <input type="checkbox" name="is_active" value="1" <?= App\Http\Controllers\Common\Helpers::checked($row->is_active, 1)?> />
                </div>
            </div>
             
             
            <div class="col-md-12 col-sm-12">
                <?php $_GET['brand_id']=$row->brand_id ?>
                <?=  \App\Http\Controllers\Common\Helpers::Brands()?>
            </div>
            
            
            <br style="clear: both">
            <div class="pull-right margin-top-10">
                <button class="btn btn-danger" type="submit">Submit</button>
            </div>
        </form>


    </div>
</div>

@stop


@section('js')
<script type="text/javascript" src="./assets/backend/js/plugins/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="./assets/backend/js/plugins/bootstrap-select/bootstrap-select.js"></script>

@stop



