@extends('backend.layout')


@section('content')
<div class="page-title">
    <h1>{{$module_name}}</h1>
    <p>Manage {{$module_name}}</p>

    <ul class="breadcrumb">
        <li><a href="./dashboard">Dashboard</a></li>
        <li><a href="./dashboard/{{$module_url}}/pages">{{$module_name}}/pages</a></li>
    </ul>
</div>

<!-- datatables plugin -->
<div class="wrapper wrapper-white">
    <div class="table-responsive">
        <table class="table table-bordered ">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>page name</th>
                    <th>page_id</th>
                    <th>brand_id</th>
                    <th>time</th>
                    <th>last post</th>
                    <th>active</th>
                    <th>Actions</th>
                </tr>
            </thead>                               
            <tbody>
                @foreach($data as $row)
                <tr>
                    <td>#{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->page_id}}</td>
                    <td>
                        --
                    </td>
                    <td>{{$row->time}}</td>
                    <td>
                        @if($row->last_post>0)
                            <?=date('Y-m-d H:i:s',$row->last_post)?>
                        @endif
                    </td>
                    <td>{{$row->is_active}}</td>
                    
                   <td>
                       <a href='https://www.facebook.com/pages/-/{{$row->page_id}}' target="_blank">
                            <button class="btn btn-info">Show Page</button>
                        </a>
                   
                        <a href='./dashboard/{{$module_url}}/manage/{{$row->id}}'>
                            <button class="btn btn-info">Manage</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        
        
    </div>

</div>                        
<!-- ./datatables plugin -->


@stop
