﻿@extends('backend.layout')
@section('content')

<div class="page-title">
    <h1>New Post</h1>
    <p>You Can Create New Post</p>

    <ul class="breadcrumb">
        <li><a href="./dashboard">Dashboard</a></li>
        <li>FB publisher</li>
        <li>Add New</li>
    </ul>
</div>

<div class="wrapper wrapper-white">

    <div class="row">
        @if(Session::has('errors'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Error !</strong> 
                    <br/>
                    {!!Session::get('errors')!!}
            </div>
        @endif 
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Success !</strong> 
                    <br/>
                    {{Session::get('success')}}
            </div>
        @endif
        
        
        <form method="post" id="validate" role="form" action="" enctype="multipart/form-data">
            <input type="hidden" name="caption" value="AutoPilot" />
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label>Post :</label>
                    <textarea name="message" class="form-control"></textarea>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">

                <div class="form-group">
                        <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultChecked" name="post_options" value="linkpart" checked>
                                <label class="custom-control-label" for="defaultChecked">Link</label>
                        </div>

                        <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultUnchecked1" name="post_options" value="mediapart">
                                <label class="custom-control-label" for="defaultUnchecked1">Attach Media Files</label>
                        </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group allparts" id="linkpart">
                    <label>Link :</label>
                    <input type="url" name="link" class="form-control"/>
                </div>
                <div class="form-group  allparts" id="mediapart" style="display:none;">
                    <label>Media files :</label>
                    <input type="file" name="file[]" class="form-control" multiple/>
                </div>
            </div>
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label>Scedule :</label>
                    <input type="date" name="time" class="form-control"/>
                </div>
            </div>  
           
            
            @foreach($pages as $key=>$page)
            @if($key%4==0 && $key!=0)
            <hr style="clear: both"/>
            @endif
            <div class="col-md-3">
                <input type="hidden" name="pages_ids[{{$key}}]" value="{{$page->page_id}}"/>
                <label class="switch">
                    <input type="checkbox" name="pages[{{$key}}]" value="{{$page->long_token}}"/>
                    <span></span>
                    {{$page->name}}
                </label>
            </div>
            
            @endforeach
            
            <br style="clear: both">
            <div class="pull-right margin-top-10">
                <button class="btn btn-info" type="submit">Post</button>
            </div>
        </form>


    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='post_options']:checked").val();
            //hide all
            $(".allparts").hide();
            $(".allparts input").prop('disabled', true);
            //show target
            $("#"+radioValue+" input").prop('disabled', false);
            $("#"+radioValue).show();
        });
    });
</script>
@stop




