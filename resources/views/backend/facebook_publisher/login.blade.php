@extends('backend.layout')

@section('content')
<div class="page-title">
    <h1>{{$module_name}}</h1>
    <p>Manage {{$module_name}}</p>

    <ul class="breadcrumb">
        <li><a href="./dashboard">Dashboard</a></li>
        <li><a href="./dashboard/{{$module_url}}">{{$module_name}}</a></li>
        <li><a href="./dashboard/{{$module_url}}/create"><button class="btn btn-info btn-clean">New {{$module_name}}</button></a></li>
    </ul>
</div>

<div class="wrapper wrapper-white">

    <div class="row">
        <br/>
        <div id="login_button" style="display:none;">
        <fb:login-button scope="public_profile,email,publish_pages,manage_pages" onlogin="checkLoginState();">
        </fb:login-button>
        </div>
        <br/>
        <div id="pages">
            <br/>
        </div>
    </div>
</div>
@stop

@section('js')
<script>

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
   
   /////////////////////////////////////////////////////////////////////////////
   
        window.fbAsyncInit = function () {
            FB.init({
                appId: '348162626103474',
                cookie: true, // enable cookies to allow the server to access 
                // the session
                xfbml: true, // parse social plugins on this page
                version: 'v4.0' // use graph api version 4.0
            });
            checkLoginState();
        };
        
        function checkLoginState() {
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        }

        function statusChangeCallback(response) {
            if (response.status !== 'connected') {
                $("#login_button").show();
            }else{
                var social_account_id = loginWithFB(response.authResponse.accessToken);
                getMyPages(social_account_id);
            }
        }

        function loginWithFB(access_token) {
            FB.api('/me', {fields: "id,name,email"}, function(response) {
              $.ajax({
                url:'./dashboard/facebook-publisher/register',
                method:'post',
                data:{
                    name:response.name,
                    email:response.email,
                    access_token:access_token,
                    social_id:response.id,
                    social_type:1
                },
                async:false,
                success: function (response) {
                     if(response.status == 0){
                         var v_errors = '';
                         for(var c in response.erros){
                            v_errors += response.erros[c];
                         }
                         alert(v_errors);
                         return false;
                     }else{
                         return response.id;
                     }
                 }
              }); 
            });
        }
        {{-- function getLongToken(social_id){
            $.ajax({
                url: './facebook/longtoken?cache=false',
                data:{social_id:social_id},
                method: 'post',
                async:false,
            });
        } --}}

        function getMyPages(social_account_id){
            FB.api('/me', {fields: "accounts,manage_pages,publish_pages"}, function (response) {
            console.log(response);
                var request=[];
                for(var i in response.data){
                    request.push({name:response.data[i].name,access_token:response.data[i].access_token,category:response.data[i].category,page_id:response.data[i].id});
                }
                $.ajax({
                   url:'./dashboard/facebook-publisher/savepages',
                   method:'post',
                   data:{social_account_id:social_account_id ,data:request},
                   async:false,
                   success: function (response) {
                        if(response.status!=200){
                            alert("sorry,Something went wrong.");
                        }else{
                            alert("Your login process is successfully completed.");
                        }
                    }
                });
            });
        }
   
   
</script>



@stop
