<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('dashboard/facebook-publisher/login','Backend\FacebookPublisherController@getLogin');
Route::post('dashboard/facebook-publisher/register','Backend\FacebookPublisherController@postRegister');
Route::post('dashboard/facebook-publisher/savepages','Backend\FacebookPublisherController@postSavepages');
Route::any('dashboard/facebook-publisher/pages','Backend\FacebookPublisherController@anyPages');
Route::any('dashboard/facebook-publisher/poster','Backend\FacebookPublisherController@anyPoster');
Route::any('dashboard/facebook-publisher/manage/{id}','Backend\FacebookPublisherController@anyManage');

Route::any('facebook/publish','Facebook\PublisherController@anyPublish');
Route::any('facebook/test','Facebook\PublisherController@anyTest');
Route::post('facebook/longtoken','Facebook\PublisherController@postLongtoken');


Route::get('/', function () {
    return view('welcome');
});
