<?php

namespace App\Http\Controllers\Facebook;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Models\SocialAccount;
use DB;

class PublisherController extends Controller {

    function anyPublish() {
        //dd('publish function');
        $pages = \App\Models\FacebookPages::where('is_active', 1)->get();
        foreach ($pages as $page) {
            $postTime = $page->last_post + $page->time * 60;
            //dd([$postTime,time()]);
            if (time() > $postTime && $page->brand_id > 0) {
                $cars = DB::table('cars')
                                ->join('cars_images', 'cars.id', '=', 'cars_images.car_id')
                                ->leftJoin('facebook_posts', 'cars.id', '=', 'facebook_posts.car_id')
                                ->where('facebook_posts.car_id', null)
                                ->where('cars.brand_id', $page->brand_id)
                                ->join('brands', 'brands.id', '=', 'cars.brand_id')
                                ->join('sub_brands', 'sub_brands.id', '=', 'cars.sub_brand_id')
                                ->select('cars.*', 'cars_images.image', 'brands.name as brand_name', 'sub_brands.name as sub_brand_name')
                                ->groupBy('cars.id')
                                ->orderBy('cars.id', 'desc')->take(1)->get();
                //dd($cars);
                if (count($cars) == 0)
                    continue;
                $car = $cars[0];
                $data['message'] = $car->brand_name . " " . $car->sub_brand_name . " " . $car->manufacture_year . " \n " . "السعر ::" . number_format($car->price);
                $data['name'] = $car->brand_name . ' - ' . $car->sub_brand_name;
                $data['description'] = $car->details;
                $data['link'] = $this->getCarUrl($car);
                $data['picture'] = 'http://www.3rabiat.com/upload/' . $car->image;
                $data['caption'] = '3rabiat.com';
                $data['access_token'] = $page->long_token;
                $post_id = $this->postToPage($page->page_id, $data);
                $page->last_post = time();
                $page->save();
                DB::table('facebook_posts')->insert(['car_id' => $car->id, 'post_id' => $post_id]);
            }
        }
    }


    function getBlog(Request $request) {

        $page = \App\Models\FacebookPages::where('page_id', 910598369003666)->first();
        $blog = \App\Models\Blog::orderBy(\DB::raw('rand()'))->first();

        $data['name'] = $blog->title;
        $data['message'] = $blog->title;
        $data['description'] = $blog->title;
        $data['link'] = $this->getBlogUrl($blog);
        $data['picture'] = 'http://www.3rabiat.com/upload/' . $blog->image;
        $data['caption'] = '3rabiat.com';
        $data['access_token'] = $page->long_token;
        $this->postToPage($page->page_id, $data);
    }

    function postToPage($page_id, $data , $type='feed') {
        if($type != "videos")
        $url = 'https://graph.facebook.com/';
        else{
        $url = 'https://graph-video.facebook.com/';
        $data['description'] = $data['message'];
        unset($data['message']);
    }
        $post_url = $url . $page_id . '/'.$type;
        unset($data['post_options']);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
        return $return;
    }

    // function postLongtoken(Request $request) {
    //     $account = SocialAccount::where('social_id',$request->social_id)->first();
    //         if ($long_token = $this->getLongAcessToken($account->access_token)) {
    //             $account->long_token = $long_token;
    //             $account->long_token_update_at = time();
    //             $account->save();
    //         }
    // }

    // function getLongAcessToken($short_token) {
    //     $linkData = [];
    //     $linkData['grant_type'] = 'fb_exchange_token';
    //     $linkData['client_id'] = '348162626103474';
    //     $linkData['client_secret'] = 'eb34163533e65cf74f5edbff1c6e1b24';
    //     $linkData['fb_exchange_token'] = $short_token;

    //     $post_url = 'https://graph.facebook.com/oauth/access_token';
    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_URL, $post_url);
    //     curl_setopt($ch, CURLOPT_POST, 1);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $linkData);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     $return = curl_exec($ch);
    //     curl_close($ch);
    //     $result = json_decode($return);

    //     if (is_object($result)) {
    //         //dd($result->access_token);
    //         return $result->access_token;
    //     }
    //     return false;

    //     if (!strstr($return, '=') && !strstr($return, '&')) {
    //         dd('ffff');
    //         return false;
    //     }

    //     $result = explode('=', $return);

    //     $result = $result[1];
    //     $result = explode('&', $result);
    //     $token = $result[0];
    //     dd($token);
    //     return $token;
    // }

    function anyIndex() {
        //$this->anyPublish();
        dd('index function');
    }

    private function getBlogUrl(\App\Models\Blog $blog) {
        return url('blog/show/' . $blog->id . '/' . urlencode(str_replace(' ', '-', $blog->title)));
    }

}
