<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FacebookPublisherController extends Controller
{

    public function __construct()
    {
        $module_url = 'facebook-publisher';
        $module_name = 'Facebook publisher';
        $user = User::find(1);
        session(['backend_user' => $user]);
        view()->share('module_url', $module_url);
        view()->share('module_name', $module_name);
    }

    public function getLogin()
    {
        return view('backend.facebook_publisher.login');
    }

    public function postRegister(Request $request)
    {
        $response = [];

        $social_account = SocialAccount::where('email', $request->email)->first();
        if (is_object($social_account) && $social_account->count() > 0) {
            $response["status"] = 200;
            $response["id"] = $social_account->id;
            return $response;
        }
        $request->merge(['user_id' => session('backend_user')->id]);
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:social_accounts',
            'social_id' => 'required|unique:social_accounts',
            'social_type' => 'required',
            'access_token' => 'required|unique:social_accounts',
            'user_id' => 'required',
        ];
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            $response["status"] = 0;
            $response["errors"] = [];
            foreach ($validation->errors()->all() as $one) {
                $response["errors"][] = $one;
            }
            return $response;
        }
        $social_account = new SocialAccount();
        $social_account->fill($request->all());
        $social_account->save();
        $long_token = $this->getLongAcessToken($social_account->access_token);
        if ($long_token !== false) {
            $social_account->long_token = $long_token;
            $social_account->long_token_update_at = time();
            $social_account->save();
        }
        $response["status"] = 200;
        $response["id"] = $social_account->id;
        return $response;
    }

    public function getLongAcessToken($short_token)
    {
        $linkData = [];
        $linkData['grant_type'] = 'fb_exchange_token';
        $linkData['client_id'] = '348162626103474';
        $linkData['client_secret'] = 'eb34163533e65cf74f5edbff1c6e1b24';
        $linkData['fb_exchange_token'] = $short_token;

        $post_url = 'https://graph.facebook.com/oauth/access_token';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $linkData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($return);

        if (is_object($result)) {
            //dd($result->access_token);
            return $result->access_token;
        }
        return false;

        // if (!strstr($return, '=') && !strstr($return, '&')) {
        //     dd('ffff');
        //     return false;
        // }

        // $result = explode('=', $return);

        // $result = $result[1];
        // $result = explode('&', $result);
        // $token = $result[0];
        // dd($token);
        // return $token;
    }

    public function postSavepages(Request $request)
    {
        if ($request->has('data')) {
            dd($request->get('data'));
            $response = [];
            
            foreach ($request->get('data') as $row) {
                $page = \App\Models\FacebookPages::where('social_page_id', $row['page_id'])->first();
                if (!is_object($page) || $page->count() == 0) {
                    $page = new \App\Models\FacebookPages();
                }
                $page->name = $row['name'];
                $page->access_token = $row['access_token'];
                $page->category = $row['category'];
                $page->social_page_id = $row['page_id'];
                $page->save();

                $social_account_page = \App\Models\SocialAccountsPage::where('page_id', $row['page_id'])->where("social_account_id", $request->social_account_id)->first();
                if (!is_object($social_account_page) || $social_account_page->count() == 0) {
                    $social_account_page = new \App\Models\SocialAccountsPage();
                    $social_account_page->page_id = $row['page_id'];
                    $social_account_page->social_account_id = $row['social_account_id'];
                    $social_account_page->save();
                }
            }
            $response["status"] = 200;
            return $response;
        }
    }

    public function anyPages()
    {
        return view('backend.facebook_publisher.pages')->with('data', \App\Models\FacebookPages::all());
    }

    public function anyManage(Request $request, $page_id)
    {
        $page = \App\Models\FacebookPages::find($page_id);
        if ($request->has('brand_id') && $request->has('time')) {
            $page->fill($request->all());
            $page->save();
            \Illuminate\Support\Facades\Session::flash('success', 'updated');
        }
        return view('backend.facebook_publisher.manage')->with('row', $page);
    }

    public function anyPoster(Request $request)
    {
        $data['pages'] = \App\Models\FacebookPages::all();
        if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $files_array = [];
                $files = $request->File('file');
                $request->request->remove('file');
                $request = $request->except('file');
                foreach ($files as $file) {
                    $files_array[] = $this->saveMedia($file);
                }
            } else {
                $request = $request->all();
            }
            // dd($request);
            // dd($files_array);

            foreach ($request as $key => $value) {
                if ($value == '' || $key == 'pages_ids' || $key == 'post_options') {
                    continue;
                }
                if ($key == 'pages') {
                    $post_pages = $value;
                } else {
                    $post_data[$key] = $value;
                }
            }
            $publisher = new \App\Http\Controllers\Facebook\PublisherController();
            foreach ($post_pages as $key => $access) {
                $post_data['access_token'] = $access;
                if (isset($files_array)) {
                    $attached_media = [];
                    $attached_media_errors = [];
                    foreach ($files_array as $one_file) {
                        $post_data["published"] = false;
                        if ($one_file['mime_type'] == 'photos') {
                            $post_data["url"] = $one_file['url'];
                        } else {
                            $post_data["file_url"] = $one_file['url'];
                        }

                        $response = $publisher->postToPage($request["pages_ids"][$key], $post_data, $one_file['mime_type']);
                        $response = json_decode($response);
                        if (isset($response->id)) {
                            $attached_media[] = $response->id;
                        } else {
                            $attached_media_errors[] = $respnse->error;
                        }

                    }
                }
                // dd($attached_media);
                if (count($attached_media) > 0) {
                    foreach ($attached_media as $key => $value) {
                        $post_data['attached_media'][$key] = json_encode(["media_fbid" => $value]);
                        // $post_data['attached_media'][$key] ='{"media_fbid":"'.$value.'"}';
                    }
                }
                $post_data["published"] = true;
                unset($post_data['url']);
                unset($post_data['file_url']);
                // dd($post_data);
                $post_id = $publisher->postToPage($request["pages_ids"][$key], $post_data, 'feed');
                echo $post_id . '<br/>';
            }
            die();
        }
        return view('backend.facebook_publisher.poster', $data);
    }

    private function saveImages($media_file)
    {
        $formats = ['png', 'jpg', 'jpeg', 'JPEG', 'JPG', 'PNG'];
        if (!in_array($media_file->getClientOriginalExtension(), $formats)) {
            return ['error' => 'Sorry, Unsupported extension for this file  ' . $media_file->getClientOriginalName()];
        }
        $mimetype = explode('/', $media_file->getClientMimeType())[0];
        if ($mimetype != 'image') {
            return ['error' => 'Sorry, Unsupported file type for this file  ' . $media_file->getClientOriginalName()];
        }
        if (!file_exists('./upload/fb-poster-media')) {
            mkdir('./upload/fb-poster-media', 0775, true);
        }
        $file_name = md5((float) (time() * random_int(1, 999999))) . '.' . $media_file->getClientOriginalExtension();
        $media_file->move('./upload/fb-poster-media', $file_name);
        return [
            'url' => 'https://mediasci.net/social-autoposting/public/upload/fb-poster-media/' . $file_name,
            'mime_type' => 'photos',
        ];
    }
    private function saveVideos($media_file)
    {
        $formats = ['3g2', '3gp', '3gpp'
            , 'asf', 'avi', 'dat', 'divx', 'dv', 'f4v', 'flv', 'gif', 'm2ts'
            , 'm4v', 'mkv', 'mod', 'mov', 'mp4', 'mpe', 'mpeg', 'mpeg4', 'mpg'
            , 'mts', 'nsv', 'ogm', 'ogv', 'qt', 'tod', 'ts', 'vob', 'wmv'];
        if (!in_array($media_file->getClientOriginalExtension(), $formats)) {
            return ['error' => 'Sorry, Unsupported extension for this file  ' . $media_file->getClientOriginalName()];
        }
        $mimetype = explode('/', $media_file->getClientMimeType())[0];
        if ($mimetype != 'video') {
            return ['error' => 'Sorry, Unsupported file type for this file  ' . $media_file->getClientOriginalName()];
        }
        if (!file_exists('./upload/fb-poster-media')) {
            mkdir('./upload/fb-poster-media', 0775, true);
        }
        $file_name = md5((float) (time() * random_int(1, 999999))) . '.' . $media_file->getClientOriginalExtension();
        $media_file->move('./upload/fb-poster-media', $file_name);
        return [
            'url' => 'https://mediasci.net/social-autoposting/public/upload/fb-poster-media/' . $file_name,
            'mime_type' => 'videos',
        ];
    }

    public function anyTest()
    {
        //$publisher=new \App\Http\Controllers\Facebook\PublisherController();
        $id = $this->postToPage('500659800107123', [
            'title' => 'title',
            'description' => 'description here',
            'link' => 'http://www.3rabiat.com',
            'thumbnail ' => 'http://www.3rabiat.com/upload/fb-poster/4f46e8c84f0ff08e689105ee3a8b48df.jpg',
            'caption' => '3rabiat.com',
            'access_token' => 'EAAY47mFyR4oBAAJAKYp5JJ80C2wnRxJEbvOzC4yYZBGEQUkPejBCVJ4nprCZA4BiTW4AZA6MYzjBymwOXmHEM1vl1SZBcT5qPEvZAq8BfFqnn0En0fCuaJjAZBZBRCSugeMIC4wCHAOARhKdz8JJo3xlYumKMHHZCM9dONLogC0cb6Wy4rCtXDM2',
        ]);
        dd($id);
    }
    public function postToPage($page_id, $data)
    {
        $post_url = 'https://graph.facebook.com/' . $page_id . '/feed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
        return $return;
    }

}
