<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsMedia extends Model
{
    protected $table='posts_media';
    public $timestamps=false;
}
