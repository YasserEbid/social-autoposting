<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class FacebookPages extends BaseModel
{
    protected $table='facebook_pages';
    public $timestamps=true;
    protected $guarded=['id'];

    public function socialAccounts(){
        return $this->belongsToMany('App\Models\SocialAccount', 'social_accounts_pages', 'page_id', 'social_account_id');
    }

    public function posts(){
        return $this->belongsToMany('App\Models\Post', 'posts_pages', 'page_id', 'post_id');
    }
    
}
