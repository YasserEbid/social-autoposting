<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table='posts';

    public function pages(){
        return $this->belongsToMany('App\Models\FacebookPages', 'posts_pages', 'post_id', 'page_id');
    }

    public function socialAccount(){
        return $this->belongsTo('App\Models\SocialAccount', 'social_account_id');
    }

    public function media(){
        return $this->hasMany('App\Models\PostsMedia', 'post_id');
    }
}
