<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsPage extends Model
{
    protected $table='posts_pages';
    public $timestamps=false;
}
