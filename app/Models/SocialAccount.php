<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    protected $table='social_accounts';

    protected $fillable = ['name','user_id','email','social_id','access_token','social_type'];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function facebookPages(){
        return $this->belongsToMany('App\Models\FacebookPages', 'social_accounts_pages', 'social_account_id', 'page_id');
    }

    public function posts(){
        return $this->hasMany('App\Models\Post','social_account_id');
    }
}
