<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccountsPage extends Model
{
    protected $table='social_accounts_pages';
    public $timestamps=false;
}
