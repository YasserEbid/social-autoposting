<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacebookPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('facebook_pages', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('name');
			$table->string('category')->nullable(true);
			$table->text('access_token', 65535)->nullable(true);
			$table->bigInteger('social_page_id')->nullable(true);
			$table->boolean('is_active')->default(1);
			// $table->integer('time')->nullable(true);
			// $table->integer('last_post')->nullable(true);
			// $table->integer('brand_id')->nullable(true);
			// $table->text('long_token', 65535)->nullable(true);
			// $table->integer('long_token_update_at')->nullable(true);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facebook_pages');
	}

}
